In lab3 I used the depth sensor  and laser scan.

In observation 1 the bot can see the shelf. It has identified as much
as the camera angle allows while showing the depth and edges of the shelf
itself

In observation 2 the bot does not see the white cube 

In observation 3 the camera and sensor's limits are tested where it can see
both the box up close and the whole shelf far away. The colors determine
how close or far the object is. The box is close so it is yellow meanwhile
the shelf is almost at the end of the range at purple

In observation 4 the laser scan is shown off meaning the bot can tell where]
the edges of objects are. In this case the edges of the sphere and barrier
are identified.
