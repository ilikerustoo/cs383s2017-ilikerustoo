public class Buyer{
    private final double CP = 100;
    Market market;
    
    public Buyer(Market m){
        this.market = m;
    }
    
    void buy(){
        double [] sPrice = new double[market.sellers.length];
        
        for(int i=0; i<market.sellers.length; i++){
            sPrice[i] = market.sellers[i].requestPrice();
        }
        
        //select the best price
        double minPrice = sPrice[0];
        int preferredSeller = 0;
        for(int i=1; i<market.sellers.length; i++){
            if ( sPrice[i] < minPrice ){
                minPrice = sPrice[i];
                preferredSeller = i;
            }
        }
        if( minPrice <= CP ){
            market.sellers[preferredSeller].selectSeller();
        }
        
    }
    
}