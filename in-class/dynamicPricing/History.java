public class History{
    double profit;
    double currentPrice;
    int timeIntervalId;
    
    public History(double p, double c, int t){
        profit = p;
        currentPrice = c;
        timeIntervalId = t;
    }
    
    public String toString(){
        return profit         +", "+
               currentPrice   +", "+
               timeIntervalId +"\n ";
    }
}