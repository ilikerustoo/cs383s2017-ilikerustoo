import lejos.hardware.motor.Motor;
import lejos.robotics.navigation.DifferentialPilot;

public class WheelCalibration {
	DifferentialPilot pilot;
	
	public static void main (String [] args)
	{
		new WheelCalibration();
	}
	public WheelCalibration()
	{
		pilot = new DifferentialPilot(0.0f, 0.0f, Motor.C, Motor.B);
		pilot.travel(100);
	}
}
