/* Rename this file as <username>.cpp, where <username> is your Andrew user 
	id. */

#include <iostream>
#include "auction.h"
#include "template.h"  /* Change this to <username>.h */


/* Currently, these functions always return NO_BID.  Rename the function as
	<username>_disc_bid_slot1 and <username>_disc_bid_slot2 and
	<username>_uniform_bid_slot1 and <username>_uniform_bid_slot2, where
	<username> is your Andrew user id, and implement your bidding strategy
	here. If you choose to participate in a particular auction, return a
	double greater than or equal to 0. If you do not want to participate in
	the auction, return NO_BID.
	*/

/* Change this function to be called <username>_disc_bid_slot1, where <username> is
	your Andrew user id. */
double disc_bid_slot1(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   ){
		   return NO_BID;
};

/* Change this function to be called <username>_disc_bid_slot2, where <username> is
	your Andrew user id. */
double disc_bid_slot2(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   ){
		   return NO_BID;
};

/* Change this function to be called <username>_uniform_bid_slot1, where <username> is
	your Andrew user id. */
double uniform_bid_slot1(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   ){
		   return NO_BID;
};


/* Change this function to be called <username>_uniform_bid_slot2, where <username> is
	your Andrew user id. */
double uniform_bid_slot2(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   ){
		   return NO_BID;
};
