#ifndef _BIDDER2_H_
#define _BIDDER2_H_

#include "auction.h"

double bidder2_disc_bid_slot1(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   );

double bidder2_disc_bid_slot2(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   );

double bidder2_uniform_bid_slot1(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   );


double bidder2_uniform_bid_slot2(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   );

#endif
