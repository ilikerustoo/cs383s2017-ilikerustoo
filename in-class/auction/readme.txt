Taken from CMU’s AI course assignment.

Most Important:
---------------
template.h, template.cpp :  These are the files that you will modify and hand
in. Rename these files as <username>.h and <username>.cpp, where <username>
is your team name.  These files contain the prototype for four functions.
You will rename these functions and then implement your bidding algorithm
in these functions.  The functions take as argument your agent's current
wealth, your agent's current valuation for the slots, the number of agents
still participating in the auction. The function returns a double that is your
agent's bid for this timestep's auction.  If you do not wish to participate in
the auction, you must return NO_BID.

auction.cpp : This file implements the auction.  You may need to make some changes
in this file for the purposes of testing. You can replace one of the bidder with
functions corresponding to your own bidder or just add extra statements for your bidder. 

Makefile : To test, you should replace files of a bidder with your files or add your files.

Test Files
----------
There are a few test agents, all of whom do stupid things.  They are 
mainly for comparing your agent to, and so that the auction mechanism can
run. You don't need to modify them.

bidder1.cpp : This agent always bids random numbers lower than its value. 

bidder2.cpp : This agent always bids exactly its current value.

bidder3.cpp : This agent always returns NO_BID, so it never participates in an
auction.

