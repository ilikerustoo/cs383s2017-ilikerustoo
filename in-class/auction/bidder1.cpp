#include <iostream>
#include "auction.h"
#include "bidder1.h"

double bidder1_disc_bid_slot1(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   ) {
  return getRandomNumber(100, my_value1);
}

double bidder1_disc_bid_slot2(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   ) {
  return getRandomNumber(0, my_value2);
}

double bidder1_uniform_bid_slot1(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   ) {
  return getRandomNumber(30, my_value1);
}

double bidder1_uniform_bid_slot2(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   ) {
  return getRandomNumber(0, my_value2);
}
