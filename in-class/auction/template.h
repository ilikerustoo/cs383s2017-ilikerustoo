/* Rename this file as <username>.h, where <username> is your Andrew user 
	id. */

/* Change the following two lines to read:
	  #ifndef _USERNAME_H_
	  #define _USERNAME_H_
	where USERNAME is your Andrew user id.
*/
#ifndef _TEMPLATE_H_
#define _TEMPLATE_H_

#include "auction.h"

/* Change this function to be called <username>_disc_bid_slot1, where <username> is
	your Andrew user id. */
double disc_bid_slot1(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   );

/* Change this function to be called <username>_disc_bid_slot2, where <username> is
	your Andrew user id. */
double disc_bid_slot2(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   );

/* Change this function to be called <username>_uniform_bid_slot1, where <username> is
	your Andrew user id. */
double uniform_bid_slot1(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   );


/* Change this function to be called <username>_uniform_bid_slot2, where <username> is
	your Andrew user id. */
double uniform_bid_slot2(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   );

#endif
