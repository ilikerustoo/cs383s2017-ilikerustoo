#ifndef _BIDDER1_H_
#define _BIDDER1_H_

#include "auction.h"

double bidder1_disc_bid_slot1(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   );

double bidder1_disc_bid_slot2(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   );

double bidder1_uniform_bid_slot1(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   );


double bidder1_uniform_bid_slot2(double my_value1,
		   double my_value2,
		   double my_money,
		   int num_bidders
		   );

#endif
