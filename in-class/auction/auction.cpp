#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include "auction.h"
#include "bidder1.h"
#include "bidder2.h"
#include "bidder3.h"

using namespace std;

#define NUM_BIDDERS 3

vector<double> wealth_disc;
vector<double> wealth_uniform;
vector<double> bid1_disc;
vector<double> bid2_disc;
vector<double> bid1_uniform;
vector<double> bid2_uniform;
vector<double> value1;
vector<double> value2;

/* This is a pseudo-random number generator.  It takes generates a random
   number in the range [low, high]. */
double getRandomNumber(double low, double high) {
	 /* If the random number generator hasn't been seeded yet, seed it. */
	 return low + ((high - low) * rand()/((double)RAND_MAX));
}

int main() {
	 /* Initialize all bidders */ 
	 for (int i=0 ; i<NUM_BIDDERS ; i++) {
		  wealth_disc.push_back(10000);
		  wealth_uniform.push_back(10000);
		  bid1_disc.push_back(0);
		  bid2_disc.push_back(0);
		  bid1_uniform.push_back(0);
		  bid2_uniform.push_back(0);
		  value1.push_back(0);
		  value2.push_back(0);
	 }

	 /* This is the main body of the auction code.  We perform the auction
		1000 times.  */
	 srand(time(NULL));
	 for (int i = 0 ; i < 1000; i++) {
		  cout << "Auction number "<< (i + 1) << endl;

		  // Compute valuation of slots for each bidder
		  for (int j = 0 ; j < NUM_BIDDERS ; j++) {
			   value1[j] = getRandomNumber(100, 200);
			   value2[j] = getRandomNumber(100, value1[j]);
		  }

		  // Find the number of bidders having wealth greater than 0
		  int num_bidders_disc = 0;
		  int num_bidders_uniform = 0;
		  for (int j = 0 ; j < NUM_BIDDERS ; j++) {
			   if ( wealth_disc[j] > 0)
					num_bidders_disc++;
			   if ( wealth_uniform[j] > 0)
					num_bidders_uniform++;
		  }

		  // For each bidder, compute the bids for uniform and disc case
		  if (wealth_disc[0] > 0) {
			   bid1_disc[0] = bidder1_disc_bid_slot1( value1[0], value2[0], wealth_disc[0], NUM_BIDDERS);
			   bid2_disc[0] = bidder1_disc_bid_slot2( value1[0], value2[0], wealth_disc[0], NUM_BIDDERS);
		  } else {
			   bid1_disc[0] = 0; 
			   bid2_disc[0] = 0;
		  }
		  if (wealth_uniform[0] > 0) {
			   bid1_uniform[0] = bidder1_uniform_bid_slot1( value1[0], value2[0], wealth_uniform[0], NUM_BIDDERS);
			   bid2_uniform[0] = bidder1_uniform_bid_slot2( value1[0], value2[0], wealth_uniform[0], NUM_BIDDERS);
		  } else {
			   bid1_uniform[0] = 0; 
			   bid2_uniform[0] = 0;
		  }
		  if (wealth_disc[1] > 0) {
			   bid1_disc[1] = bidder2_disc_bid_slot1( value1[1], value2[1], wealth_disc[1], NUM_BIDDERS);
			   bid2_disc[1] = bidder2_disc_bid_slot2( value1[1], value2[1], wealth_disc[1], NUM_BIDDERS);
		  } else {
			   bid1_disc[1] = 0; 
			   bid2_disc[1] = 0;
		  }
		  if (wealth_uniform[1] > 0) {
			   bid1_uniform[1] = bidder2_uniform_bid_slot1( value1[1], value2[1], wealth_uniform[1], NUM_BIDDERS);
			   bid2_uniform[1] = bidder2_uniform_bid_slot2( value1[1], value2[1], wealth_uniform[1], NUM_BIDDERS);
		  } else {
			   bid1_uniform[1] = 0; 
			   bid2_uniform[1] = 0;
		  }
		  if (wealth_disc[2] > 0) {
			   bid1_disc[2] = bidder3_disc_bid_slot1( value1[2], value2[2], wealth_disc[2], NUM_BIDDERS);
			   bid2_disc[2] = bidder3_disc_bid_slot2( value1[2], value2[2], wealth_disc[2], NUM_BIDDERS);
		  } else {
			   bid1_disc[2] = 0; 
			   bid2_disc[2] = 0;
		  }
		  if (wealth_uniform[2] > 0) {
			   bid1_uniform[2] = bidder3_uniform_bid_slot1( value1[2], value2[2], wealth_uniform[2], NUM_BIDDERS);
			   bid2_uniform[2] = bidder3_uniform_bid_slot2( value1[2], value2[2], wealth_uniform[2], NUM_BIDDERS);
		  } else {
			   bid1_uniform[2] = 0; 
			   bid2_uniform[2] = 0;
		  }

		  /* disc auction */ 
		  double max_bid1 = 0, max_bid2 = 0;
		  int index1 = 0, index2 = 0;
		  for (int j = 0 ; j < NUM_BIDDERS ; j++) {
			   if (bid1_disc[j] != 0 || bid2_disc[j] != 0)
					wealth_disc[j] -= 2;
			   if (bid1_disc[j] > max_bid1) {
			        index2 = index1;
					max_bid2 = max_bid1;
					index1 = j;
					max_bid1 = bid1_disc[j];
			   } else if (bid1_disc[j] > max_bid2) {
					index2 = j;
					max_bid2 = bid1_disc[j];
			   }
			   if (bid2_disc[j] > max_bid1) {
					index2 = index1;
					max_bid2 = max_bid1;
					index1 = j;
					max_bid1 = bid2_disc[j];
			   } else if (bid2_disc[j] > max_bid2) {
					index2 = j;
					max_bid2 = bid2_disc[j];
			   }
			   //cout << bid1_disc[j] << " " << bid2_disc[j] << endl;

		  }
		  //cout << "winner " << index1 << ": " << max_bid1 << " " << index2 <<
		  //" " << max_bid2 << endl;

		  if (max_bid1 > 0) {
			   wealth_disc[index1] = wealth_disc[index1] + value1[index1] - max_bid1;
			   if (wealth_disc[index1] < 0)
					wealth_disc[index1] = 0;
		  }
		  if (max_bid2 > 0) {
		       if (index1 == index2)
					wealth_disc[index2] = wealth_disc[index2] + value2[index2] - max_bid2;
			   else
					wealth_disc[index2] = wealth_disc[index2] + value1[index2] - max_bid2;
			   if (wealth_disc[index2] < 0)
					wealth_disc[index2] = 0;
		  }

		  max_bid1 = 0, max_bid2 = 0;
		  index1 = 0, index2 = 0;

		  /* uniform auction */ 
		  for (int j = 0 ; j < NUM_BIDDERS ; j++) {
			   if (bid1_uniform[j] != 0 || bid2_uniform[j] != 0)
					wealth_uniform[j] -= 2;
			   if (bid1_uniform[j] > max_bid1) {
					index2 = index1;
					max_bid2 = max_bid1;
					index1 = j;
					max_bid1 = bid1_uniform[j];
			   } else if (bid1_uniform[j] > max_bid2) {
					index2 = j;
					max_bid2 = bid1_uniform[j];
			   }
			   if (bid2_uniform[j] > max_bid1) {
					index2 = index1;
					max_bid2 = max_bid1;
					index1 = j;
					max_bid1 = bid2_uniform[j];
			   } else if (bid2_uniform[j] > max_bid2) {
					index2 = j;
					max_bid2 = bid2_uniform[j];
			   }
			   //cout << bid1_uniform[j] << " " << bid2_uniform[j] << endl;
		  }
		  //cout << "winner " << index1 << ": " << max_bid1 << " " << index2 <<
		  //" " << max_bid2 << endl;

		  double price = 0;           
		  for (int j = 0 ; j < NUM_BIDDERS ; j++) {
			   if ( bid1_uniform[j] != max_bid1 && max_bid2 != bid1_uniform[j] && bid1_uniform[j] > price)
					price = bid1_uniform[j];
			   if ( bid2_uniform[j] != max_bid1 && max_bid2 != bid2_uniform[j] && bid2_uniform[j] > price)
					price = bid2_uniform[j];
		  }
		  //cout << price << endl;

		  if (max_bid1 > 0) {
			   wealth_uniform[index1] = wealth_uniform[index1] + value1[index1] - price;
			   if (wealth_uniform[index1] < 0)
					wealth_uniform[index1] = 0;
		  }
		  if (max_bid2 > 0) {
		  if (index1==index2)
			   wealth_uniform[index2] = wealth_uniform[index2] + value2[index2] - price;
		  else
			   wealth_uniform[index2] = wealth_uniform[index2] + value1[index2] - price;
		  if (wealth_uniform[index2] < 0)
					wealth_uniform[index2] = 0;
		  }

	 }

	 /* Print out the final wealths of all the remaining agents and find the
		agent with the greatest wealth */

	 for (int j = 0 ; j < NUM_BIDDERS ; j++) {
		  cout << "Auction Disc bidders wealth :" << endl;
		  cout << "Wealth of bidder " << (j+1) << " " << wealth_disc[j] << endl;
	 }
	 for (int j = 0 ; j < NUM_BIDDERS ; j++) {
		  cout << "Auction uniform bidders wealth :" << endl;
		  cout << "Wealth of bidder " << (j+1) << " " << wealth_uniform[j] << endl;
	 }
}
